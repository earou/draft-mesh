# Status Web View Page
  
### 如何客製系統架構圖

__產生系統架構圖檔__
  1. 利用繪圖軟體或是投影片軟體繪製架構圖(或手繪白板拍照)，並存檔或螢幕截圖(檔案格式：`png`，範例：web/imgs/sysview.png)
  2. 替換 `web/imgs/sysview.png`
  3. 使用瀏覽器開啟 `web/index.html` 確認底圖已被替換

__調整 Health Status 節點位置__
  1. 編輯 `web/index.html`
  
  以下區塊標記每個節點的位置，`web-f1` 為節點名稱，`top` 和 `left` 值決定該節點在畫面上會出現的位置。請反覆修改嘗試。
  ```html
   <style>
      #web-f1 {
        position: absolute;
        top: 24%;
        left: 25%;
      }
      #f1-b1 {
        position: absolute;
        top: 24%;
        left: 59%;
      }
      #f1-f2 {
        position: absolute;
        top: 37%;
        left: 42%;
      }
      #f2-b1 {
        position: absolute;
        top: 49.8%;
        left: 59%;
      }
      #app-f2 {
        position: absolute;
        top: 69.7%;
        left: 38%;
      }
      #app-b1 {
        position: absolute;
        top: 79%;
        left: 38%;
      }
    </style>
  ```

  如有修改節點名稱，必須在以下定義區塊修改對應的 id 名稱。可以自由增加或減少節點數量。
  ```html
      <div class="check-point bad" id="web-f1">BAD</div>
      <div class="check-point bad" id="f1-b1">BAD</div>
      <div class="check-point bad" id="f1-f2">BAD</div>
      <div class="check-point bad" id="f2-b1">BAD</div>
      <div class="check-point bad" id="app-f2">BAD</div>
      <div class="check-point bad" id="app-b1">BAD</div>
  ```

  ### 部署至 AWS S3

  1. 在 AWS S3 上新增一個 bucket（如果需要 custom domain 必須以 domain name 命名 bucket name。）
  2. 將 `web` 資料夾底下的檔案上傳至剛剛新增的 bucket。
  3. 啟用 AWS S3 Static Hosting 選項。
  4. 此時如果這個 bucket 底下所有的檔案權限已經設為 public，可直接透過 S3 提供的 endpoint 瀏覽 index.html 頁面。

  ### 限制特定 IP 來源存取

  如系統狀態不想對外公開，建議設定 S3 Bucket Policy。
  
  限制 IP address 的 Policy 範例如下：

```json
{ 
    "Version": "2012-10-17",
    "Id": "Policy1234678", 
    "Statement": [
        {  
            "Sid": "Access-to-specific-VPCE-only",
            "Effect": "Allow",
            "Principal": "*", 
            "Action": "s3:GetObject",
            "Resource": [
                "arn:aws:s3:::{web view bucket name}",
                "arn:aws:s3:::{web view bucket name}/*"
            ],
            "Condition": {
                "IpAddress": { 
                    "aws:SourceIp": [
                        "{ip addr1}/32",
                        "{ip addr2}/32"
                    ]
                }
            }
        }
    ] 
} 
``` 
  
限制 VPC Gateway Endpoint 可參考 [Build private static website on S3](https://blog.monsterxx03.com/2017/08/19/build-private-staticwebsite-on-s3/)。