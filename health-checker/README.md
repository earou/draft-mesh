# Health Checker

### 基本設置

編輯 `health-checker/serverless.html` 在 yml 結構底下開頭區塊。範例如下。
```yaml
service: SystemStatus

provider:
  name: aws
  runtime: nodejs6.10
  region: ap-northeast-1
  stage: dev
  environment:
    S3_BUCKET: "{s3 bucket name}"
```

`service:` 為 serverless 交付識別名稱，執行後會在 AWS CLoudforamtion 建立一個名為 `{stage}-{service name}` 的 stack。

`region:` 請依照你想放置的 AWS Region 做修改。

`S3_BUCKET:` 請設置 Static Web View Page 部署的 S3 bucket name。

### 設定檢查節點

編輯 `health-checker/serverless.html` 在 yml 結構底下 functions 區塊。範例如下。
```yaml
web-f1:
    handler: simple-http-health-checker.check
    events:
      - schedule:
          rate: cron(* * * * ? *)
          enabled: true
          input:
            check_id: web-f1
            check_url: "http://{f1 endpoint}/health"
```
function 名稱 `web-f1` 命名上沒有限制，方便識別即可。 

`check_id` 和在 `web/index.html` 設定的節點名稱必須一致。

`check_url` 請設定檢查對象的 healthchecker endpoint

如果 `check_url` 僅供 VPC 內部存取，請加上 vpc 區塊，範例如下：
```yaml
  f1-b1:
    handler: simple-http-health-checker.check
    events:
      - schedule:
          rate: cron(* * * * ? *)
          enabled: true
          input:
            check_id: f1-b1
            check_url: "http://{b1 endpoint}/health"
    vpc:
      securityGroupIds:
        - {security group id}
      subnetIds:
        - {private subnet id}
        - {private subnet id}
```

### 部署

在 `health-checker` 資料夾底下執行以下指令：
```shell
$ serverless deploy
```

並可在 AWS Cloudformation console 檢查實際上建立了哪些資源。