'use strict';

const AWS = require('aws-sdk')
const S3 = new AWS.S3()
const https = require('https')
const http = require('http')
const url = require('url')

module.exports.check = (event, context, callback) => {
  request(event.check_url, () => {
    markHealthyTimestamp(process.env.S3_BUCKET, event.check_id)
  })
};

function markHealthyTimestamp(s3_bucket, check_id) {
  let dateTime = Date.now()
  let current_time_in_sec = Math.floor(dateTime / 1000)

  S3.upload({
    Bucket: s3_bucket,
    Key: 'checks/' + check_id,
    Body: ''+current_time_in_sec
  }, (err, res) => {
    console.log(err, res)
  })
}

function request( urlStr, callback_200 ) {
  const urlObj = url.parse(urlStr)

  if( urlObj.protocol.indexOf('https') > -1 ) {
      https.get(urlStr, (resp) => {
          if (resp.statusCode == 200 ) {
              callback_200()
          }
        }).on("error", (err) => {
          console.log("Error: " + err.message);
        });
  } else {
      http.get(urlStr, (resp) => {
          if (resp.statusCode == 200 ) {
              callback_200()
          }
        }).on("error", (err) => {
          console.log("Error: " + err.message);
        });
  }
}