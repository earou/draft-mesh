'use strict';

const AWS = require('aws-sdk')
const S3 = new AWS.S3()
const http = require('http');

module.exports.check = (event, context, callback) => {
  let healthy_interval = 60 // in secs
  let dateTime = Date.now()
  let current_time_in_sec = Math.floor(dateTime / 1000)
  S3.listObjectsV2({
    Bucket: process.env.S3_BUCKET,
    Prefix: 'checks'
  }, (err, res) => {
    if (res.Contents) {
      const length = res.Contents.length
      let count = 0
      let errCount = 0
      res.FileContents = {}
      res.Contents.forEach((item) => {
        const key = item.Key
        S3.getObject({
          Bucket: process.env.S3_BUCKET,
          Key: key,
        }, (err, data) => {
          count++
          data.body = data.Body.toString()
          console.log("ts: " + data.body);
          
          if ( (current_time_in_sec - parseInt(data.body)) > healthy_interval ) {
            console.log("ERROR")
            errCount++
          }

          if (count === length) {
            if(errCount > 0) {
              resAlarm( callback, event )
            } else {
              resNormal(callback, event)
            }

          }
        })
      })
    } else {
      callback(err, res)
    }
  })
};


function resAlarm (callback, event) {
  const response = {
    statusCode: 500,
    body: JSON.stringify({
      message: 'Something wrong!!',
      input: event,
    }),
  };

  callback(null, response);
}

function resNormal (callback, event) {
  const response = {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Ok!!',
      input: event,
    }),
  };

  callback(null, response);
}