# draft-mesh

`附註：`僅適用於架設在 AWS 上的應用系統

本專案實作 DevOps 精神。用來快速建立一個可描述應用系統內部的各節點相依關係的 Web View Page。

特別適用於還沒導入 [Service Mesh](https://www.cncf.io/blog/2017/04/26/service-mesh-critical-component-cloud-native-stack/) 的專案，除了架構圖，還提供了各節點即時健康狀況資訊，相信對於系統層面的除錯溝通可以有一定程度的幫助。

本專案包含兩個部分。

### 1. Status Web View

  * 一個靜態 HTML 頁面包含一張滿版的手繪架構圖
  * 利用 CSS 標註架構圖中每個節點的健康狀態
  * 每隔 10 秒刷新一次節點的健康狀態
  * 架設在 AWS S3 並使用 [Static Hosting](https://docs.aws.amazon.com/AmazonS3/latest/dev/WebsiteHosting.html)
  * DEMO

### 2. Health Checkers

  * 使用 [Serverless](https://serverless.com) Framework
  * 使用 AWS Lambda 進行節點的 Health Check (目前僅支援 HTTP & HTTPS)
  * 可選擇將 Lambda 放置於 VPC 內網或外網，不僅可以檢查對外節點，對內節點也可以。
  * 基本使用僅需對 serverless.yml 進行設置
  * `(未完成)`另有一隻單獨的 Lambda 檢查所有的節點回報值，若有任意節點失敗便主動通知。

# 設置與部署

請先將 `Status Web View` 部署完成後，再進行 `Health Checkers` 的部署。

1. [Config & Deploy Guide](web/README.md) for `Status Web View `
2. [Config & Deploy Guide](health-checker/README.md) for `Health Checkers`

# FAQ

* 可以將 Status Web View 設定為僅供內網瀏覽嗎？

可以的，在 AWS S3 Bucket 設置 Access Policy 限定特定 IP 或 VPC Gateway Endpoint 即可。

限制 IP address 的 Policy 範例如下：

```
{
    "Version": "2012-10-17",
    "Id": "Policy1234678",
    "Statement": [
        {
            "Sid": "Access-to-specific-VPCE-only",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": [
                "arn:aws:s3:::{web view bucket name}",
                "arn:aws:s3:::{web view bucket name}/*"
            ],
            "Condition": {
                "IpAddress": {
                    "aws:SourceIp": [
                        "{ip addr1}/32",
                        "{ip addr2}/32"
                    ]
                }
            }
        }
    ]
}
```

限制 VPC Gateway Endpoint 可參考 [Build private static website on S3](https://blog.monsterxx03.com/2017/08/19/build-private-staticwebsite-on-s3/)。


  

